package com.nannapat.week8;

public class CircleShape {
    private String name;
    private double r;

    public CircleShape(String name, double r) {
        this.name = name;
        this.r = r;

    }

    public double areaCircle() {
        double area = 3.14 * r * r;
        System.out.println("Radius : " + r);
        System.out.println("Area of circle : " + area);
        System.out.println();
        return area;
    }

    public double perimeterCircle() {
        double perimeter = 2 * 3.14 * r;
        System.out.println("Radius : " + r);
        System.out.println("Perimeter of circle : " + perimeter);
        System.out.println();
        return perimeter;
    }

    public String getName() {
        return name;
    }

    public double getR() {
        return r;
    }
}
