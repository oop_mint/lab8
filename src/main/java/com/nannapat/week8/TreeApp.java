package com.nannapat.week8;

public class TreeApp {
    public static void main(String[] args) {
        TestTree tree1 = new TestTree("Tree1", 5, 10);
        TestTree tree2 = new TestTree("Tree2", 5, 11);

        tree1.print();
        tree2.print();
    }
}
