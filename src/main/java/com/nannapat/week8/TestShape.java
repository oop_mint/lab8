package com.nannapat.week8;

public class TestShape {
    public static void main(String[] args) {
        RectangleShape rect1 = new RectangleShape("rect1 : ", 10, 5);
        RectangleShape rect2 = new RectangleShape("rect2 : ", 5, 3);
        CircleShape circle1 = new CircleShape("circle1 : ",1);
        CircleShape circle2 = new CircleShape("circle2 : ",2);
        TriangleShape triangle1 = new TriangleShape("triangle1 : ",5, 5, 6);

        rect1.areaRectangle();
        rect2.areaRectangle();
        rect1.perimeterRectangle();
        rect2.perimeterRectangle();

        circle1.areaCircle();
        circle2.areaCircle();
        circle1.perimeterCircle();
        circle2.perimeterCircle();
        
        triangle1.areaTriangle();
        triangle1.perimeterTriangle();
    }
}
