package com.nannapat.week8;

public class TestTree {
    private String name;
    private int x;
    private int y;
    public final static int x_min = 0;
    public final static int x_max = 19;
    public final static int y_min = 0;
    public final static int y_max = 19;

    public TestTree(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public void print() {
        System.out.println(name + " x: " + x + " y: " + y);
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
